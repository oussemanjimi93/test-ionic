import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-informations',
  templateUrl: './informations.component.html',
  styleUrls: ['./informations.component.scss'],
})
export class InformationsComponent implements OnInit {
data:any;
  constructor(public navParams: NavParams,private modal:ModalController) {
    this.data = this.navParams.get('params')
   }

  ngOnInit() {
  }
  dismiss(){
    this.modal.dismiss()
  }
}
