import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InformationsComponent } from '../Modals/informations/informations.component';
import { PersonneService } from '../services/personne.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  listPerson:any;
  constructor(private personneService:PersonneService,public modalController: ModalController) {}

  ngOnInit() {
    this.getList()
  }
  getList(){
    this.personneService.getPersonList().subscribe((list)=>{
      this.listPerson = list
    })
  }

  async infoPerson(person) {
    const modal = await this.modalController.create({
      component: InformationsComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        params: person,
        other: { couldAlsoBeAnObject: true }
      }
    });
    return await modal.present();
  }
  search(event){
    
    console.log(event.detail.value)
    if (event.detail.value == ""){
      this.getList()
    }
    this.listPerson = this.listPerson.filter(p=>p.first.includes(event.detail.value))
  }
}
