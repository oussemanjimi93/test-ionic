import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {

  constructor(public http: HttpClient) { }

  getPersonList(){
    return this.http.get("https://run.mocky.io/v3/484607a5-cd8e-4909-b73a-ac123e437b89")
  }
}
